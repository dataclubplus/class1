# Imports
from collections import deque

# read/write from file

# 1. Data structure:

# 1.1 String "" '':
# -----------------
word = 'Python'

print(word[0]) # Char in position 0

print(word[-1]) # Char in last position

print(word[-2]) # second-last char

print(word[0:2]) # Char up to 2 (excluded)

print(word[:2]) # Same

print(word[2:5]) # 2 inclusive 5 exclusive

# Python strings cannot be changed — they are immutable. Therefore, assigning to an indexed position in the string
# results in an error:

# word[0] = 'J'

# If you need a different string, you should create a new one:

newWord = 'J' + word[1:]
print(newWord)

# this is also good, why?
word = 'J' + word[1:]
print(word)

# 1.2 List []:
# ------------

# Python knows a number of compound data types, used to group together other values. The most versatile is
# the list, which can be written as a list of comma-separated values (items) between square brackets. Lists might
# contain items of different types, but usually the items all have the same type.

squares = [1, 4, 9, 16, 25]

print(squares[0])

print(squares[-1])

print(squares[-3:])

# What is this?
newOrNotNewSquares = squares[:]

newOrNotNewSquares[0] = 9788375

print(squares, ' ?= ', newOrNotNewSquares)

# are lists mutable or immutable?

# Lists also support operations like concatenation:
newSquares = squares + [36, 49, 64, 81, 100]
print(newSquares)

# You can also add new items at the end of the list, by using the append() method (we will see more about methods later):
cubes = [1, 8, 27, 64, 125]
cubes.append(7 ** 3)
print(cubes)
cubes.append(8 ** 3)
print(cubes)

# what's happening here?
print(cubes.append(9 ** 3))

# Assignment to slices is also possible, and this can even change the size of the list or clear it entirely:
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']

letters[2:5] = ['C', 'D', 'E']
print(letters)

# What is this?
letters[2:5] = []
print(letters)

# The built-in function len() also applies to lists:
print(len(letters))

# It is possible to nest lists (create lists containing other lists), for example:
a = ['a', 'b', 'c']
n = [1, 2, 3]
x = [a, n]
print(x)

# What is this?
#Error
#print(x[1][3])

# And this?
print(x[1][0])

# List methods: count, index, reverse, append, sort, pop,

fruits = ['orange', 'apple', 'pear', 'banana', 'kiwi', 'apple', 'banana']

# count
print(fruits.count('apple'))
print(fruits.count('tangerine'))

# index
print(fruits.index('banana'))
print(fruits.index('banana', 4))  # Find next banana starting a position 4

# reverse
# what happened here?
print(fruits.reverse())
print(fruits)

# sort
fruits.sort()
print(fruits)

# pop
fruits.pop()
print(fruits)

# Stacks(LIFO) vs Queue(FIFO)
stack = [3, 4, 5]
stack.append(6)
print(stack)
stack.pop()
print(stack)

queue = deque([3, 4, 5])
queue .append(6)
print(queue)

# what happened here?
queue.pop()
print(queue)


queue .append(6)
print(queue)
queue.popleft()
print(queue)

# 1.3 Tuple ():
# -------------

tuple = 3, 4, 5
print(tuple)

# What will happen here?
# tuple[2] = 6

tuple = 3, 4, [5, 6]
print(tuple)

# will that work?
tuple[2][0] = 7
print(tuple)

# 1.4 Sets {}:
# -------------

# Python also includes a data type for sets. A set is an unordered collection with no duplicate elements. Basic uses
# include membership testing and eliminating duplicate entries. Set objects also support mathematical operations like
#  union, intersection, difference, and symmetric difference.
basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
print(basket) # show that duplicates have been removed
print('orange' in basket)

a = set('abracadabra')
print(a)

# what will happen here?
a1 = {'abracadabra'}
print(a1)


b = set('alacazam')
print(a - b) # letters in a but not in b
print(a | b) # letters in a or b or both
print(a & b) # letters in both a and b
print(a ^ b) # letters in a or b but not both


# What is this? (high language support)
a = {x for x in 'abracadabra' if x not in 'abc'}
print(a)

# 1.5 Dictionaries/dict {:,}:
# ---------------------------

# 'associative arrays',unordered set of key: value pairs, with the requirement that the keys are unique (within one
# dictionary). A pair of braces creates an empty dictionary: {}. Placing a comma-separated list of key:value pairs
# within the braces adds initial key:value pairs to the dictionary; this is also the way dictionaries are written on
# output.

tel = {'jack': 4098, 'sape': 4139}
tel['guido'] = 4127
print(tel)

# keys
print(tel.keys())

# what will happen?
# print(basket.keys())

# what will happen?
tel['jack'] = 0
print(tel)

# how can I change key to something else?


# keys are immutable dict are not

# solution
del tel['jack']
tel['jackie'] = 0
print(tel)

print('jack' in tel)
print('jackie' in tel)

# dict constructor the oop way
newTel = dict([('jackie', 0), ('sape', 4139), ('guido',4127)])
print(newTel)


# 2. Control flow

# 2.1 If:
x = 5
if x > 5:
    print('G')
elif x < 5:
    print('L')
elif x == 5:
    print('E')
else:
    print('Alien')

# 2.2 For:
list = ['cat', 'window', 'defenestrate'] # List []
set = {'cat', 'window', 'defenestrate'} # set {}
tuple = 'cat', 'window', 'defenestrate' # tuple ,
dict = {'cat': 0, 'window': 1, 'defenestrate': 2} # dict {:,}

for l in list[:]:
    print(l)

for l in list:
    print(l)

for s in set:
    print(s)

for t in tuple:
    print(t)


# what will happen?
for d in dict:
    print(d)

for k, v in dict.items():
    print(k, v)



for i, v in enumerate(list):
    print(i, v)
    # enumerate - return tuple containing the count

for s in sorted(set):
    print(s)


# inclusive or exclusive?
for num in range(2, 10):
    print(num)

# Functions

# def func(n):
# func(0)

def func(n):
    pass

func(0)

# default arguments


def func(n):
    print(n)

# Lambda Expressions (x: x + n)
# Small anonymous functions can be created with the lambda keyword

# Example 1:
# without Lambda
def filterfunc(x):
    return x % 3 == 0
mult3 = filter(filterfunc, range(10))
for x in mult3:
    print(x)

# with lambda
mult3 = filter(lambda x: x % 3 == 0, range(10))
for x in mult3:
    print(x)

# Example 2:
# without Lambda
def get_value(pair):
    return pair[1]

pairs = [(1, 'one'), (2, 'two'), (3, 'three'), (4, 'four')]
pairs.sort(key=get_value)
print(pairs)

# with lambda
pairs = [(1, 'one'), (2, 'two'), (3, 'three'), (4, 'four')]
print(pairs)
pairs.sort(key=lambda pair: pair[1])
print(pairs)

# it works because you give the process that run sort a reference to the function and not the function itself than sort
# uses the function somewhere in the background and call it with the relevant arguments

# Class

class MyClass:
    number = 12345

    def f(self):
        print(self.number)
        return 'hi'

x = MyClass()
print(x.number)
print(x.f())


